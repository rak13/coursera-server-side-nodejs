const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const NAME = 'promotion';
const NAME_PLURAL = NAME + 's';
const NAME_CAP = 'Promotion';
const NAME_ID = 'promoId';

const Model = require(`../models/${NAME_PLURAL}`);

const router = express.Router();
router.use(bodyParser.json());

router.route('/')
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        next();
    })
    .get((req, res, next) => {
        Model.find({})
        .then((results) => {
            res.json(results)
        }, (err) => next(err))
        .catch((err) => next(err));
    })
    .post((req, res, next) => {
        Model.create(req.body)
        .then((result) => {
            console.log(`${NAME_CAP} Created `, result);
            res.json(result);
        }, (err) => next(err))
        .catch((err) => next(err));
    })
    .put((req, res, next) => {
        res.statusCode = 403;
        res.setHeader('Content-Type', 'text/plain');
        res.end(`PUT operation not supported on /${NAME_PLURAL}`);
    })
    .delete((req, res, next) => {
        Model.remove({})
        .then((result) => {
            res.json(result);
        }, (err) => next(err))
        .catch((err) => next(err));   
    });

router.route(`/:${NAME_ID}`)
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        next();
    })
    .get((req, res, next) => {
        Model.findById(req.params[NAME_ID])
        .then((result) => {
            res.json(result);
        }, (err) => next(err))
        .catch((err) => next(err));
    })
    .post((req, res, next) => {
        res.statusCode = 403;
        res.setHeader('Content-Type', 'text/plain');
        res.end(`POST operation not supported on /${NAME_PLURAL}/${req.params[NAME_ID]}`);
    })
    .put((req, res, next) => {
        Model.findByIdAndUpdate(req.params[NAME_ID], {
            $set: req.body
        }, { new: true })
        .then((result) => {
            res.json(result);
        }, (err) => next(err))
        .catch((err) => next(err));
    })
    .delete((req, res, next) => {
        Model.findByIdAndRemove(req.params[NAME_ID])
        .then((result) => {
            res.json(result);
        }, (err) => next(err))
        .catch((err) => next(err));
    });

module.exports = router;