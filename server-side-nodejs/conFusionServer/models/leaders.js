const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ModelName = 'Leader'

const ModelSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    image: {
        type: String,
        required: true
    },
    designation: {
        type: String,
        required: true
    },
    abbr: {
        type: String,
        default: ''
    },
    featured: {
        type: Boolean,
        default:false      
    },
    description: {
        type: String,
        required: true
    }
}, {
    timestamps: true
});

var model = mongoose.model(ModelName, ModelSchema);

module.exports = model;