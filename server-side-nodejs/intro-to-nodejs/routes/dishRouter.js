const express = require('express');
const bodyParser = require('body-parser');

const NAME = 'dish';
const NAMES = 'dishes';
const idNAME = 'dishId';

const router = express.Router();
router.use(bodyParser.json());
router.route('/')
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();
    })
    .get((req, res, next) => {
        res.end(`Will send all the ${NAMES} to you!`);
    })
    .post((req, res, next) => {
        res.end(`Will add the ${NAME}: ${req.body.name}  with details: ${req.body.description}`);
    })
    .put((req, res, next) => {
        res.statusCode = 403;
        res.end(`PUT operation not supported on /${NAMES}`);
    })
    .delete((req, res, next) => {
        res.end(`Deleting all ${NAMES}`);
    });

router.route(`/:${idNAME}`)
    .get((req, res, next) => {
        console.log(req);
        res.end(`Will send details of the ${NAME}:  ${req.params[idNAME]} to you!`);
    })
    .post((req, res, next) => {
        res.end(`Will add the ${NAME}: ${req.body.name} with details: ${req.body.description}`);
    })
    .put((req, res, next) => {
        res.write(`Updating the ${NAME}: ${req.params[idNAME]} \n`);
        res.end(`Will update the ${NAME}:  ${req.body.name} with details: ${req.body.description}`);
    })
    .delete((req, res, next) => {
        res.end(`Deleting ${NAME}: ${req.params[idNAME]}`);
    });

module.exports = router;